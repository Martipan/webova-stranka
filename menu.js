// Funkce pro vytvoření menu
function createMenu() {
    var menuContainer = document.getElementById("menu-container");
  
    // Vytvoření hlavičky
    var header = document.createElement("header");
  
    // Vytvoření navigačního prvku
    var nav = document.createElement("nav");
  
    // Vytvoření seznamu
    var menuList = document.createElement("ul");
  
    // Vytvoření položek menu
    var menuItems = ["Domů", "O nás", "FAQ", "Kontakty"];
    var pageUrls = ["domu.html", "o_nas.html", "faq.html", "kontakty.html"];
    
    for (var i = 0; i < menuItems.length; i++) {
      var menuItem = document.createElement("li");
      var menuLink = document.createElement("a");
      
      // Přidání atributu href s cílovou stránkou
      menuLink.href = pageUrls[i];
      
      menuLink.textContent = menuItems[i];
      menuItem.appendChild(menuLink);
      menuList.appendChild(menuItem);
    }
  
    // Přidání seznamu do navigačního prvku
    nav.appendChild(menuList);
  
    // Přidání navigačního prvku do hlavičky
    header.appendChild(nav);
  
    // Přidání hlavičky do kontejneru
    menuContainer.appendChild(header);
  }
  
  // Volání funkce pro vytvoření menu při načtení stránky
  window.onload = createMenu;
  
  